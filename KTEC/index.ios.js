import React, { Component } from 'react'
import {
  AppRegistry,
  StyleSheet,
  TabBarIOS,
  //Text
} from 'react-native'
import AppNavigator from './app/navigation/AppNavigator'
import Icon from 'react-native-vector-icons/FontAwesome'


class KTEC extends Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedTab: "hometab"
    }
    //console.ignoredYellowBox = ['Remote debugger']
  }

  render() {
    return (
      <TabBarIOS
        selectedTab={this.state.selectedTab}
        //unselectedTintColor={'black'}
        tintColor={'royalblue'}
        unselectedItemTintColor={'whitesmoke'}
        barTintColor={'skyblue'}>

        <Icon.TabBarItem
          selected={this.state.selectedTab === 'hometab'}
          title={'Home'}
          iconName={'home'}
          onPress={() => this.setState({ selectedTab: "hometab" })}>
          <AppNavigator
            initialRoute={{ ident: "HomeIndex" }} />
        </Icon.TabBarItem>

        <Icon.TabBarItem
          selected={this.state.selectedTab === 'appointtab'}
          title={'Appointments'}
          iconName={'calendar'}
          onPress={() => {
            this.setState({ selectedTab: "appointtab" })
          }}>
          <AppNavigator
            initialRoute={{ ident: "AppointmentsIndex" }}
          />
        </Icon.TabBarItem>

        <Icon.TabBarItem
          selected={this.state.selectedTab === 'procedtab'}
          title={'Procedures'}
          iconName={'list-ul'}
          onPress={() => this.setState({ selectedTab: "procedtab" })}
        >
          <AppNavigator
            initialRoute={{ ident: "ProceduresIndex" }} />
        </Icon.TabBarItem>

        <Icon.TabBarItem
          selected={this.state.selectedTab === 'profiletab'}
          title={'Profile'}
          iconName="user"
          //iconSize={25}
          onPress={() => this.setState({ selectedTab: "profiletab" })}
        >
          <AppNavigator
            initialRoute={{ ident: "ProfileIndex" }}
          />
        </Icon.TabBarItem>

      </TabBarIOS>
    )
  }
  
}


const styles = StyleSheet.create({

});

AppRegistry.registerComponent('KTEC', () => KTEC);
