import React, { Component } from 'react'
import {
  //  StyleSheet,
  Text,
  AlertIOS,
  View,
  TouchableHighlight,
  //  TouchableOpacity,
  ListView,
  //  Navigator
} from 'react-native'

import ViewContainer from 'KTEC/app/components/ViewContainer/ViewContainer'
import StatusBar from 'KTEC/app/components/StatusBar/StatusBar'
import Loading from 'KTEC/app/components/Loading/Loading'
//import Icon from 'react-native-vector-icons/FontAwesome'
import styles from './styles'
import api from '../../services/api'


var proceduresArray = []

class ProceduresIndex extends Component {
  constructor(props) {
    super(props)
    var ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 != r2 })
    this.state = {
      ds: ds.cloneWithRows(proceduresArray),
      isLoading: true
    }
  }

  componentDidMount() {
    api.getProcedures().then((responseData) => {
      proceduresArray = responseData
      this.setState({
        ds: this.state.ds.cloneWithRows(proceduresArray),
        isLoading: false
      })
    })
      .catch((err) => {
        console.log(err)
      })
  }

  _renderRow(rowData, sectionID, rowID) {
    return (
      //Idea: onPress= get more info || take me to new appointment?
      <View style={styles.treatmentView}>
        <Text style={styles.treatmentText}> {rowData.name} </Text>
        <Text style={styles.treatmentText}> {'\t\t\t\t'} ${rowData.price}</Text>
      </View>
    );
  }

  render() {
    var currentView = (this.state.isLoading) ? <Loading /> : <ListView style={styles.listView} dataSource={this.state.ds} renderRow={this._renderRow.bind(this)} enableEmptySections={true} />

    return (
      <ViewContainer>
        <StatusBar />
        <Text style={styles.title}>Dental Care Fees</Text>
        {currentView}
      </ViewContainer>
    );
  }

}


module.exports = ProceduresIndex