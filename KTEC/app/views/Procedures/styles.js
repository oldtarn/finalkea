import { StyleSheet } from 'react-native'
const styles = StyleSheet.create({
    title: {
        padding: 10,
        fontSize: 25,
        textAlign: 'center',
        textDecorationLine: 'underline',
        textDecorationColor: '#C7DFF0',
    },
    listView: {
        marginBottom: 50,

    },
    treatmentView: {
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: '#C7DFF0',
        marginLeft: 20,
        paddingVertical: 5
    },
    treatmentText: {
        flex: 1,
        fontSize: 18,
    }
})

module.exports = styles