import React, { Component } from 'react'
import {
    Text,
    TextInput,
    View,
    TouchableOpacity,
    AsyncStorage,
    AlertIOS
} from 'react-native'


import Icon from 'react-native-vector-icons/FontAwesome'
import styles from './styles'
import api from '../../services/api'

//TODO: display save button only when changes are made?
class ProfileView extends Component {
    constructor(props) {
        super(props)
        this.state = {
            firstname: "Insert your firstname",
            lastname: "Insert your lastname",
            address: "Insert your address",
            phone: "Insert phone number",
            username: "You",
            avatar: null,
            method: null
        }
    }

    componentDidMount() {
        api.getProfile().then((responseJson) => {
            if (responseJson.length != 0) {
                this.setState({
                    //idea: api should retrieve username?
                    username: (responseJson[0].firstname),
                    firstname: (responseJson[0].firstname),
                    lastname: (responseJson[0].lastname),
                    address: (responseJson[0].address),
                    phone: (responseJson[0].phonenumber),
                    avatar: (responseJson[0].profilepicture),
                    method: "PUT"
                })
            }
            else (
                this.setState({ method: "POST" })
            )
        }
        )
    }


    render() {
        //console.log(this.props)
        var avatar = (this.state.avatar === null) ?
            <TouchableOpacity
                style={styles.profilePicture}
                onPress={() => this._uploadPicture()}
            >
                <Icon
                    name='camera'
                    size={60}
                />
                <Icon
                    name='plus-square'
                    size={15}
                    color='royalblue'
                    style={styles.plusIcon}
                />
            </TouchableOpacity> :
            <Text>Avatar goes here</Text>
        return (
            <View>
                <View style={styles.topContainer}>
                    <TouchableOpacity
                        style={styles.saveButton}
                        onPress={() => this._saveChanges()}
                    >
                        <Text style={styles.saveText}>Save changes</Text>
                    </TouchableOpacity>
                    {avatar}
                    <Text style={styles.username}> {this.state.username} </Text>
                </View>
                <Text>Name</Text>
                <View style={styles.nameContainer}>
                    <TextInput
                        style={styles.textInput}
                        onChangeText={(text) => this.setState({ firstname: text })}
                        value={this.state.firstname}
                    />
                    <TextInput
                        style={styles.textInput}
                        onChangeText={(text) => this.setState({ lastname: text })}
                        value={this.state.lastname}
                    />
                </View>
                <Text>Full address</Text>
                <TextInput
                    style={styles.textInput}
                    onChangeText={(text) => this.setState({ address: text })}
                    value={this.state.address}
                />
                <Text>Telephone number</Text>
                <TextInput
                    style={styles.textInput}
                    onChangeText={(text) => this.setState({ phone: text })}
                    value={this.state.phone.toString()}
                />
                {/*TODO: can change password!
                AlertIOS.prompt( 'Change password', 'Enter current password', [ {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'}, {text: 'Save', onPress: password => console.log( + password)}, ], 'secure-text' );
                */}
                <TouchableOpacity
                    style={styles.logoutButton}
                    onPress={() => this._logout()}
                >
                    <Text style={styles.logoutText}>LOGOUT</Text>
                </TouchableOpacity>
            </View >
        )
    }
    _saveChanges() {
        api.editprofile(this.state.firstname, this.state.lastname, this.state.address, this.state.phone, this.state.method).then((responseJson) => AlertIOS.alert(responseJson.message))

    }

    _logout() {
        AsyncStorage.removeItem('token').then(
            this.props.tokenState(false)
        )
    }

    _uploadPicture() {
        //TODO: select picture and add into db (base64 or serverlink - less secure?)
        console.log('saving picture...')
    }
}

module.exports = ProfileView