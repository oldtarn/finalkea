import React, { Component } from 'react'
import {
  Text,
  AlertIOS,
  AsyncStorage
} from 'react-native'

import ViewContainer from 'KTEC/app/components/ViewContainer/ViewContainer'
import StatusBar from 'KTEC/app/components/StatusBar/StatusBar'
import StatusUser from 'KTEC/app/components/StatusUser/StatusUser'
import ProfileView from 'KTEC/app/views/Profile/profileView'
import api from '../../services/api'
import styles from './styles'


var apiToken

class ProfileIndex extends Component {
  constructor(props) {
    super(props)
  }


  // shouldComponentUpdate(nextProps) {
  //   return (this.props.isTokenValid != nextProps.isTokenValid) 
  // }

  componentDidMount() {
    //TODO: show loading meanwhile?
    this._checkToken()
  }

  render() {
    //{...} - read-only
    var currentView = (this.props.isTokenValid) ? <ProfileView {...this.props} /> : <StatusUser {...this.props} />
    return (
      <ViewContainer>
        <StatusBar />
        {currentView}
      </ViewContainer>
    )
  }

  _checkToken() {
    apiToken = api.checkToken().then(
      (object) => {
        if (!!object) {
          apiToken = object.token
          AsyncStorage.getItem('token').then((token) => {
            this.props.tokenState(token === apiToken)
          })
        }
      }
    )
  }
}

module.exports = ProfileIndex