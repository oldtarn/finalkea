import { StyleSheet } from 'react-native'
const styles = StyleSheet.create({
    topContainer: {
        backgroundColor: '#C6DFEE',
        height: 185
    },
    saveButton: {
        margin: 10,
        alignItems: 'flex-end'
    },
    saveText: {
        color: 'royalblue',
        fontWeight: 'bold',
        fontSize: 16
    },
    profilePicture: {
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'flex-end',
        paddingHorizontal: 20,
        paddingVertical: 10
    },
    plusIcon: {
        marginLeft: -12,
    },
    username: {
        fontWeight: 'bold',
        fontSize: 16,
        textAlign: 'center',
        color: 'royalblue'
    },
    logoutButton: {
        height: 40,
        backgroundColor: '#E85A5A',
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 20,
        marginVertical: 110,
        borderRadius: 50
    },
    logoutText: {
        color: 'white',
        fontWeight: 'bold'
    },
    textInput: {
        marginHorizontal: 20,
        marginVertical: 5,
        height: 35,
        borderColor: '#C6DFEE',
        borderWidth: 1,
        paddingLeft: 10
    },

})

module.exports = styles