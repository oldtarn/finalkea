import { StyleSheet } from 'react-native'
const styles = StyleSheet.create({
    detailsContainer: {
        padding: 10,
        height: 185,
        backgroundColor: "#C7DFF0"
    },
    logo: {
        alignSelf: 'center',
        flexDirection: 'row',
        alignItems: 'center'
    },
    contactContainer: {
        padding: 10,
    },
    contact: {
        padding: 3,
        flexDirection: 'row',
        alignItems: 'center'
    },
    listView: {
        marginBottom: 50
    },
    tweetContainer: {
        borderBottomWidth: 6,
        borderBottomColor: '#C7DFF0'
    },
    tweetText: {
        fontSize: 16,
        color: '#000000',
        padding: '4%',
        textAlign: 'justify'
    }
})

module.exports = styles