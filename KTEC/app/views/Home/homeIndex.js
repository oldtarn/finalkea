import React, { Component } from 'react'
import {
  Text,
  AlertIOS,
  TouchableHighlight,
  View,
  ListView,
} from 'react-native'

import ViewContainer from 'KTEC/app/components/ViewContainer/ViewContainer'
import StatusBar from 'KTEC/app/components/StatusBar/StatusBar'
import Loading from 'KTEC/app/components/Loading/Loading'
import HomeDetails from './homeDetails'
import api from '../../services/api'
import styles from './styles'


var postsArray = []

class HomeIndex extends Component {
  constructor(props) {
    super(props)
    var ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 != r2 })
    this.state = {
      ds: ds.cloneWithRows(postsArray),
      isLoading: true
    }
  }

  componentDidMount() {
    api.getFeed().then((responseData) => {
      //check if response has message, otherwise don't insert
      postsArray = responseData.posts.data.filter(obj => obj.message)
      this.setState({
        ds: this.state.ds.cloneWithRows(postsArray),
        isLoading: false
      })
    })
      .catch((err) => {
        AlertIOS.alert("OOPS! Request failed.\n Check your internet connection")
        console.log(err)
      })
  }


  _renderRow(rowData, sectionID, rowID) {
    return (
      //Idea: onPress get me to post || comment inApp
      <TouchableHighlight>
        <View style={styles.tweetContainer}>
          <Text style={styles.tweetText}> {rowData.message} </Text>
        </View>
      </TouchableHighlight>
    );
  }

  render() {
    var currentView = (this.state.isLoading) ? <Loading /> : <ListView 
    style={styles.listView} 
    dataSource={this.state.ds} 
    renderRow={this._renderRow.bind(this)} 
    enableEmptySections={true}
    />

    return (
      <ViewContainer>
        <StatusBar/>
        <HomeDetails/>
        {currentView}
      </ViewContainer>
    )
  }
}

module.exports = HomeIndex