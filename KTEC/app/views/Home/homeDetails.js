import React, { Component } from 'react'
import { View, Text } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

import styles from './styles'


class homeDetails extends Component {

    render() {
        return (
            <View style={styles.detailsContainer}>
                <View style={styles.logo}>
                    <Icon name='user-md' size={50} color='white' />
                    <Text>Template Office Logo</Text>
                </View>
                <View style={styles.contactContainer} >
                    <View style={styles.contact}>
                        <Icon name='envelope' size={20} />
                        <Text> example@example.com</Text>
                    </View>
                    <View style={styles.contact}>
                        <Icon name='phone' size={20} />
                        <Text> +00 0000 0000</Text>
                    </View>
                    <View style={styles.contact}>
                        <Icon name='map-marker' size={20} />
                        <Text> Ex 101 A, ExampleLand, Example</Text>
                    </View>
                    <View style={styles.contact}>
                        <Icon name='safari' size={20} />
                        <Text> example.com</Text>
                    </View>
                </View>
            </View>
        )
    }
}


module.exports = homeDetails

