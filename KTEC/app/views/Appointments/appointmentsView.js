import React, { Component } from 'react'
import {
    Text,
    View,
    TouchableOpacity,
    ListView,
    Navigator,
    //   AsyncStorage
} from 'react-native'


import Loading from 'KTEC/app/components/Loading/Loading'
import Icon from 'react-native-vector-icons/FontAwesome'
import api from '../../services/api'
import styles from './styles'

var moment = require('moment')
var appointments = []
var pastApts = []
var ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 != r2 })


class appointmentsView extends Component {
    constructor(props) {
        super(props)
        this.state = {
            appointmentsDS: ds.cloneWithRows(appointments),
            isLoading: false,
            isActive: true,
        }
    }

    componentDidMount() {
        this._fetchAppoint()
    }

    render() {
        var currentView = (this.state.isLoading) ? <Loading /> : <View style={{ marginBottom: 128 }}>
            <Text style={styles.appointmentYear}>{moment().get('year')}</Text>
            <ListView dataSource={this.state.appointmentsDS}
                renderRow={(appointments) => { return this._renderAppointmentsRow(appointments) }} enableEmptySections />
        </View>
        return (
            <View style={{ flex: 1 }}>
                <View style={styles.delimiterContainer} >
                    <TouchableOpacity
                        onPress={() => {
                            this.setState({
                                appointmentsDS: ds.cloneWithRows(appointments),
                                isActive: true
                            })
                        }}
                        style={
                            [styles.delimiterButton, (this.state.isActive) ? styles.selected : null]
                        } >
                        <Text style={styles.delimiterText}>
                            Upcoming
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => this.setState({
                            appointmentsDS: ds.cloneWithRows(pastApts),
                            isActive: false
                        })}
                        style={
                            [styles.delimiterButton, (this.state.isActive) ? null : styles.selected]
                        } >
                        <Text style={styles.delimiterText}>
                            Past
                        </Text>
                    </TouchableOpacity>
                </View>
                {currentView}
                <TouchableOpacity style={styles.addButton} onPress={(event) => this._navigateToNewAppointment()}>
                    <Text style={styles.addText}>+</Text>
                </TouchableOpacity>
            </View>
        )
    }

    _renderAppointmentsRow(appointments) {
        var year = (appointments.year != moment().get('year')) ? <Text style={styles.appointmentYear}>{appointments.year} </Text> : null
        return (
            <View>
                {year}
                <Text style={styles.appointmentMonth}>{appointments.month}</Text>
                <TouchableOpacity style={styles.appointmentsRow} onPress={(event) => this._navigateToAppointment(appointments)}>
                    <View style={styles.appointmentDate}>
                        <Text>{(appointments.weekday).toUpperCase()}</Text>
                        <Text style={styles.dayDate}>{appointments.weeknumber}</Text>
                    </View>
                    <View style={styles.appointmentDetail}>
                        <Text style={styles.appointmentType}>{appointments.name}</Text>
                        <Text style={styles.appointmentTime}>{appointments.time}</Text>
                    </View>
                    <View style={{ flex: 1 }} />
                    <Icon name="chevron-right" style={styles.appointmentsIcon} />
                </TouchableOpacity>
            </View>
        )
    }
    _navigateToAppointment(appointments) {
        this.props.navigator.push({
            ident: "AppointmentDetail",
            appointments,
            callback: this._updateParent
        })
    }
    _navigateToNewAppointment() {
        this.props.navigator.push({
            ident: "NewAppointment",
            sceneConfig: Navigator.SceneConfigs.VerticalUpSwipeJump,
            callback: this._updateParent
        })
    }
    _fetchAppoint() {
        api.getAppointments().then(
            (responseJson) => {
                if (responseJson.lenght != 0) {
                    pastApts = []
                    responseJson.forEach((el) => {
                        if (!moment(el.date_time).isSameOrAfter(moment().format(), 'minute')) {
                            pastApts.push(el)
                            responseJson = responseJson.slice(1)
                        }
                        el.month = moment(el.date_time).format('MMMM')
                        el.year = moment(el.date_time).format('YYYY')
                        el.weekday = moment(el.date_time).format('ddd')
                        el.weeknumber = moment(el.date_time).format('Do')
                        el.time = moment(el.date_time).format('HH:mm')
                        el.date_time = moment(el.date_time).format('YYYY-MM-DD HH:mm:ss')
                    }),
                        appointments = responseJson
                    this.setState({
                        appointmentsDS: ds.cloneWithRows(responseJson),
                        isLoading: false
                    })
                }
            }
        )
    }
    _updateParent = () => {
        setTimeout(() => {
            this._fetchAppoint()
        }, 500)
    }
}


module.exports = appointmentsView