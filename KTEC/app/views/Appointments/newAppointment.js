import React, { Component } from 'react'
import {
    DatePickerIOS,
    Text,
    View,
    TouchableOpacity,
    Picker,
    AlertIOS,
} from 'react-native'

var moment = require('moment')

import StatusBar from 'KTEC/app/components/StatusBar/StatusBar'
import api from '../../services/api'
import styles from './styles'

class newAppointment extends Component {
    constructor(props) {
        super(props)
        this.state = {
            date: new Date(),
            procedures: [],
            selected: '',
            key: 1
        }
    }

    componentDidMount() {
        var list = []
        api.getProcedures().then((proc) => {
            proc.forEach((el) => { list.push(el.name) })
            return list
        }
        ).then((list) => this.setState({ procedures: list }))
    }

    render() {
        return (
            <View>
                <StatusBar />
                <TouchableOpacity style={styles.cancelButton} onPress={() => { this.props.navigator.pop() }
                }><Text style={styles.cancelText}>Cancel</Text></TouchableOpacity>
                <Text>Select date:</Text>
                <DatePickerIOS
                    date={this.state.date}
                    onDateChange={this._onDateChange.bind(this)}
                    minuteInterval={30}
                />
                <Text>Select type:</Text>
                <Picker selectedValue={this.state.selected} onValueChange={(selectedText, key) => {
                    this.setState({ selected: selectedText, key: key + 1 })
                }}>
                    {this.state.procedures.map((value, i) => <Picker.Item key={i} value={value} label={value} />)}
                </Picker>
                <TouchableOpacity style={styles.actionButton} onPress={this._saveAppt}>
                    <Text style={styles.actionText}>Save</Text>
                </TouchableOpacity>
            </View>
        )
    }
    _onDateChange(date) {
        this.setState({ date: date })

    }

    _saveAppt = () => {
        api.newAppointment(moment(this.state.date).format('YYYY-MM-DD HH:mm:00'), this.state.key).then((response) => AlertIOS.alert(response.message)).then(this.props.navigator.pop(), this.props.callback())
    }
}


module.exports = newAppointment