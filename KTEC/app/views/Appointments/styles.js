import { StyleSheet } from 'react-native'
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    appointmentsRow: {
        justifyContent: 'flex-start',
        flexDirection: 'row',
        backgroundColor: '#eaeaea',
        paddingLeft: 15,
        height: 50
    },
    appointmentsIcon: {
        color: 'white',
        fontSize: 35,
        marginRight: 5,
        alignSelf: 'center'
    },
    appointmentDate: {
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        borderRightWidth: 5,
        borderRightColor: 'lightgreen',
        width: 60
    },
    appointmentDetail: {
        justifyContent: 'space-around',
        paddingHorizontal: 20
    },
    appointmentType: {
        fontSize: 18,
        fontFamily: 'Georgia',
        fontWeight: '200'
    },
    appointmentTime: {
        color: 'royalblue'
    },
    appointmentYear: {
        textAlign: 'center',
        fontSize: 20
    },
    appointmentMonth: {
        backgroundColor: 'lightgray',
        padding: 5,
        marginVertical: 5,
        paddingLeft: 15
    },
    dayDate: {
        fontWeight: 'bold'
    },
    delimiterButton: {
        borderWidth: 1,
        borderRadius: 3,
        paddingVertical: 3,
        paddingHorizontal: 5,
        marginVertical: 15,
        borderColor: 'royalblue'
    },
    delimiterContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        backgroundColor: '#C6DFEE'
    },
    selected: {
        backgroundColor: 'white'
    },
    delimiterText: {
        color: 'royalblue'
    },
    addButton: {
        position: 'absolute',
        alignSelf: 'flex-end',
        borderWidth: 1,
        borderRadius: 50,
        borderColor: '#C5CAED',
        backgroundColor: '#C5CAED',
        top: 525,
        right: 3,
        paddingHorizontal: 16,
        paddingVertical: 4
    },
    addText: {
        color: 'white',
        fontSize: 40,
    },
    backButton: {
        position: 'absolute',
        top: 35,
        zIndex: 1,
    },
    singleType: {
        height: 100,
        backgroundColor: '#C6DFEE',
        fontFamily: 'Times New Roman',
        textAlign: 'center',
        paddingTop: 40,
        fontSize: 26,
        color: 'white',
        fontWeight: 'bold'
    },
    singleDate: {
        backgroundColor: 'gray',
        color: 'white',
        padding: 5,
        fontSize: 12,
        fontWeight: 'bold'
    },
    status: {
        backgroundColor: 'lightgreen',
        alignSelf: 'flex-end',
        zIndex: 1,
        top: -25,
        padding: 5.5,
        fontSize: 12,
    },
    singleTime: {
        height: 100,
        fontSize: 60,
        textAlign: 'center',
    },
    mapContainer: {
        height: 200,
        backgroundColor: 'lightgray'
    },
    actionButton: {
        height: 40,
        backgroundColor: '#E85A5A',
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 20,
        marginVertical: 30,
        borderRadius: 50
    },
    actionText: {
        color: 'white',
        fontWeight: 'bold'
    },
    cancelButton: {
        margin: 10,
        alignItems: 'flex-start'
    },
    cancelText: {
        color: 'royalblue',
        fontWeight: 'bold',
        fontSize: 16
    },
})

module.exports = styles