import React, { Component } from 'react'
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ListView,
  Navigator,
  AsyncStorage
} from 'react-native'


import ViewContainer from 'KTEC/app/components/ViewContainer/ViewContainer'
import StatusBar from 'KTEC/app/components/StatusBar/StatusBar'
import StatusUser from 'KTEC/app/components/StatusUser/StatusUser'
import AppointmentsView from './appointmentsView'
import Icon from 'react-native-vector-icons/FontAwesome'
import api from '../../services/api'
import styles from './styles'


var apiToken


class appointmentsIndex extends Component {
  constructor(props) {
    super(props)
  }

  shouldComponentUpdate(nextProps) {
    //console.log(this.props.isTokenValid != nextProps.isTokenValid)
    return (this.props.isTokenValid != nextProps.isTokenValid)
  }

  componentDidMount() {
    this._checkToken()
  }

  render() {
    var currentView = (this.props.isTokenValid) ?
      <AppointmentsView {...this.props} />
      : <StatusUser {...this.props} />

    return (
      <ViewContainer>
        <StatusBar />
        {currentView}
      </ViewContainer>
    );
  }

  //TODO: make it global
  _checkToken() {
    apiToken = api.checkToken().then(
      (object) => {
        if (!!object) {
          apiToken = object.token
          AsyncStorage.getItem('token').then((token) => {
            this.props.tokenState(token === apiToken)
          })
        }
      }
    )
  }
}

module.exports = appointmentsIndex