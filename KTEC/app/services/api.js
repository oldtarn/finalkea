import { AsyncStorage, AlertIOS } from 'react-native'
//dev.facebook app registration
const appID = '1810242595961762'
const appSecret = 'febe261a8d6ef967bb0ecfdfa83a6e61'

//links
const fbAppToken = 'https://graph.facebook.com/oauth/access_token?type=client_cred&client_id=' + appID + '&client_secret=' + appSecret
const dentalFB = 'https://graph.facebook.com/410763795669830?fields=posts.limit(7){created_time,message}&access_token='
const login = 'https://nodeapi.ch:3033/login'
const register = 'https://nodeapi.ch:3033/register'
const usrtoken = 'https://nodeapi.ch:3033/user'
const pricelist = 'https://nodeapi.ch:3033/pricelist'
const usrprofile = 'https://nodeapi.ch:3033/user/profile'
const usrappoint = 'https://nodeapi.ch:3033/user/appointment'


const failAlert = () => AlertIOS.alert("OOPS! Request failed.\n Check your internet connection")
var priv = {
    _checkToken(token) {
        return fetch(usrtoken,
            {
                'method': 'GET',
                headers: { token: token }
            })
            .then((response) => response.json())
            .catch((error) => { failAlert(), console.error(error) })
    }
}


var api = {
    getFeed() {
        return fetch(fbAppToken, { "method": "GET" })
            .then((response) => response.json())
            .then((responseJson) => { return responseJson.access_token })
            .then((token) => {
                return fetch(dentalFB + token, { "method": "GET" })
                    .then((response) => response.json())
            })
            .catch((error) => { failAlert(), console.error(error) })
    },
    getProcedures() {
        return fetch(pricelist, { "method": "GET" })
            .then((response) => response.json())
            .catch((error) => { failAlert(), console.error(error) })
    },
    login(usr, pwd) {
        return fetch(login, { "method": "POST", body: JSON.stringify({ user: usr, pass: pwd }) })
            .then((response) => response.json())
            .catch((error) => { failAlert(), console.error(error) })
    },
    register(newusr, newpwd) {
        return fetch(register, { "method": "POST", body: JSON.stringify({ user: newusr, pass: newpwd }) })
            .then((response) => response.json())
            .catch((error) => { failAlert(), console.error(error) })
    },
    async checkToken() {
        try {
            var validToken = await priv._checkToken(await AsyncStorage.getItem('token'))
            if (validToken !== null) { return validToken }
        } catch (error) { return null }
    },
    getProfile() {
        return AsyncStorage.getItem('token').then((token) => fetch(usrprofile, { method: "GET", headers: { token: token } })
            .then((response) => response.json())
            .catch((error) => { failAlert(), console.error(error) }))
    },
    editprofile(firstname, lastname, address, phone, method) {
        return AsyncStorage.getItem('token').then((token) => fetch(usrprofile, {
            method: method,
            headers: { token: token },
            body: JSON.stringify({
                firstname: firstname,
                lastname: lastname,
                address: address,
                phoneno: Number(phone)
            })
        })
            .then((response) => response.json())
            .catch((error) => { failAlert(), console.error(error) }))
    },
    getAppointments() {
        return AsyncStorage.getItem('token').then((token) => fetch(usrappoint, { method: "GET", headers: { token: token } })
            .then((response) => response.json())
            .catch((error) => { failAlert(), console.error(error) }))
    },
    newAppointment(datetime, type) {
        return AsyncStorage.getItem('token').then((token) => fetch(usrappoint, {
            method: "POST",
            headers: { token: token },
            body: JSON.stringify({
                datetime: datetime,
                type: type
            })
        })
            .then((response) => response.json())
            .catch((error) => { failAlert(), console.error(error) }))
    },
    deleteAppointment(datetime) {
        return AsyncStorage.getItem('token').then((token) => fetch(usrappoint, {
            method: "DELETE",
            headers: { token: token },
            body: JSON.stringify({
                datetime: datetime
            })
        })
            .then((response) => response.json())
            .catch((error) => { failAlert(), console.error(error) }))
    },

}


module.exports = api
