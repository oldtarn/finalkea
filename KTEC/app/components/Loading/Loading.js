import React, { Component } from 'react'
import { View, Text } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

import styles from './styles'


class Loading extends Component {
    render() {
        return (
            <View style={styles.view}>
                <Icon style={styles.icon}
                    name='bolt'
                    size={70}
                />
                <Text style={styles.text}>Please wait...</Text>
            </View>
        )
    }
}


module.exports = Loading