import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  view: {
    flex:1,
    justifyContent: "center",
    alignItems: "center"
  },
  icon: {
    color: "royalblue"
  },
  text:{
    color:"gray",
    fontSize: 12
  }
})

module.exports = styles