import React, { Component } from 'react'
import {
    View,
    Text,
    TouchableOpacity,
    TextInput,
    AlertIOS
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

import ViewContainer from 'KTEC/app/components/ViewContainer/ViewContainer'
import StatusBar from 'KTEC/app/components/StatusBar/StatusBar'
import styles from './styles'
import api from '../../services/api'


class Register extends Component {
    constructor(props) {
        super(props)
        this.state = {
            newUser: '',
            newPass: '',
            retypePass: '',
            responseJson: []
        }
    }
    render() {
        return (
            <ViewContainer >
                <StatusBar />
                <TouchableOpacity onPress={() => this.props.navigator.pop()}>
                    <Icon name="times" size={30} />
                </TouchableOpacity>
                <View style={styles.view}>
                    <TextInput style={styles.input}
                        placeholder="Username"
                        autoCapitalize="none"
                        autoCorrect={false}
                        maxLength={30}
                        onChangeText={(newUser) => { newUser = newUser.replace(/\s/g, ''), this.setState({ newUser }) }}
                        value={this.state.newUser}
                    />

                    <TextInput style={styles.input}
                        placeholder="Password*"
                        secureTextEntry={true}
                        autoCapitalize="none"
                        autoCorrect={false}
                        maxLength={30}
                        onChangeText={(newPass) => { newPass = newPass.replace(/\s/g, ''), this.setState({ newPass }) }}
                        value={this.state.newPass}
                    />
                    <TextInput style={styles.input}
                        placeholder="Retype password"
                        secureTextEntry={true}
                        autoCapitalize="none"
                        autoCorrect={false}
                        maxLength={30}
                        onChangeText={(retypePass) => { retypePass = retypePass.replace(/\s/g, ''), this.setState({ retypePass }) }}
                        value={this.state.retypePass}
                    />

                    <TouchableOpacity onPress={() => {
                        //TODO: handle pass mistype
                        if (this.state.newPass === this.state.retypePass) {
                            api.register(this.state.newUser, this.state.newPass)
                                .then((responseJson) => { this.setState({ responseJson }) }), setTimeout(function () { (this.state.responseJson.succes) ? (AlertIOS.alert(JSON.stringify(this.state.responseJson.message) + '. You may now proceed to login'), this.props.navigator.pop()) : (AlertIOS.alert(JSON.stringify(this.state.responseJson.message))) }.bind(this), 500)
                        }
                    }}>
                        <Text>Register</Text>
                    </TouchableOpacity>
                    <Text style={styles.infoText}>*must contain at least 8 characters including minimum a number/special character and a capital letter </Text>

                </View>
            </ViewContainer>
        )
    }
}


module.exports = Register