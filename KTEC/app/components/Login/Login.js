import React, { Component } from 'react'
import {
    View,
    TextInput,
    TouchableOpacity,
    Text,
    AlertIOS,
    AsyncStorage
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'


import ViewContainer from 'KTEC/app/components/ViewContainer/ViewContainer'
import StatusBar from 'KTEC/app/components/StatusBar/StatusBar'
import api from 'KTEC/app/services/api'
import styles from './styles'


class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: '',
            responseJson: {}
        }
    }

    render() {
        return (
            <ViewContainer>
                <StatusBar />
                <TouchableOpacity onPress={() => {
                    this.props.navigator.pop()
                }} >
                    <Icon name="times" size={30} />
                </TouchableOpacity>
                <View style={styles.view}>
                    <TextInput style={styles.input}
                        placeholder="Username"
                        autoCapitalize="none"
                        autoCorrect={false}
                        maxLength={30}
                        onChangeText={(username) => { username = username.replace(/\s/g, ''), this.setState({ username }) }}
                        value={this.state.username}
                    />

                    <TextInput style={styles.input}
                        placeholder="Password"
                        secureTextEntry={true}
                        autoCapitalize="none"
                        autoCorrect={false}
                        maxLength={30}
                        onChangeText={(password) => { password = password.replace(/\s/g, ''), this.setState({ password }) }}
                        value={this.state.password}
                    />
                    <TouchableOpacity onPress={() => {
                        //TODO: handle network error 
                        api.login(this.state.username, this.state.password).then(
                            (responseJson) => {
                                (responseJson.succes) ?
                                    (AsyncStorage.setItem(
                                        'token',
                                        responseJson.token
                                    ),
                                        this.props.route.callback(true),
                                        this.props.navigator.pop()) :
                                    (AlertIOS.alert(JSON.stringify(responseJson.message)))
                            }
                        )
                    }}>
                        <Text>Login</Text>
                    </TouchableOpacity>
                </View>

            </ViewContainer>
        )
    }
}


module.exports = Login