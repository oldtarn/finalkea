import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    view: {
        flex: 1,
        backgroundColor: "white",
        justifyContent: "center",
        alignItems: "center",
        padding: 30
    },
    input:{
        height: 40,
        borderColor: "black",
        borderWidth: 1,
        padding: 5,
        margin: 10
    }
})

module.exports = styles