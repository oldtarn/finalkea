import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    viewContainer: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    infoText: {
        fontSize: 20,
        color: "royalblue",
        fontWeight: "bold",
        padding: 20,
        textAlign: "center"
    },
    button:{
        backgroundColor: '#C7DFF0',
        margin: 10,
        padding: 5
    },
    buttonText:{
        fontSize: 16
    }
})

module.exports = styles