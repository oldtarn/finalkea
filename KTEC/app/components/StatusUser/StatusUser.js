import React, { Component } from 'react'
import { View, Text, TouchableOpacity, Navigator } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

import styles from './styles'


class StatusUser extends Component {
    constructor(props) {
        super(props)
    }
    

    render() {
        return (
            <View style={styles.viewContainer}>
                <Text style={styles.infoText}>Login or Register to manage appointments with us...{'\n'} because is that simple!</Text>
                <TouchableOpacity style={styles.button}
                    onPress={() => this._navigateToLogin()}>
                    <Text style={styles.buttonText}> Login </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button}
                    onPress={() => this._navigateToRegister()}>
                    <Text style={styles.buttonText}> Register </Text>
                </TouchableOpacity>
            </View>
        )

    }

    _navigateToLogin() {
        this.props.navigator.push({
            ident: "Login",
            sceneConfig: Navigator.SceneConfigs.FloatFromBottom,
            callback: this.callbackFunct
        })
    }
    _navigateToRegister() {
        this.props.navigator.push({
            ident: "Register",
            sceneConfig: Navigator.SceneConfigs.FloatFromBottom
        })
    }
    
    callbackFunct = (args) => {
        this.props.tokenState(args)
    }

}


module.exports = StatusUser