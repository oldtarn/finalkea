import React, { Component } from 'react'
import {
  Navigator
} from 'react-native'

import HomeIndex from 'KTEC/app/views/Home/homeIndex'
import AppointmentsIndex from 'KTEC/app/views/Appointments/appointmentsIndex'
import AppointmentDetail from 'KTEC/app/views/Appointments/appointmentDetail'
import ProceduresIndex from 'KTEC/app/views/Procedures/proceduresIndex'
import ProfileIndex from 'KTEC/app/views/Profile/profileIndex'
import Login from 'KTEC/app/components/Login/Login'
import Register from 'KTEC/app/components/Register/Register'
import NewAppointment from 'KTEC/app/views/Appointments/newAppointment'
import styles from './styles'


class AppNavigator extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isTokenValid: false
    }
  }

  _renderScene(route, navigator) {
    var globalNavigatorProps = { navigator }

    switch (route.ident) {
      case "HomeIndex":
        return (<HomeIndex {...globalNavigatorProps} />)
      case "AppointmentsIndex":
        return (<AppointmentsIndex
          {...globalNavigatorProps}
          tokenState={this.handleTokenState}
          {...this.mainState}
        />)
      case "AppointmentDetail":
        return (<AppointmentDetail
          {...globalNavigatorProps}
          appointments={route.appointments}
          {...route}
        />)
      case "ProceduresIndex":
        return (<ProceduresIndex {...globalNavigatorProps} />)
      case "ProfileIndex":
        return (<ProfileIndex
          {...globalNavigatorProps}
          tokenState={this.handleTokenState}
          {...this.mainState}
        />)
      case "Login":
        return (<Login
          route={route}
          {...globalNavigatorProps}
        />)
      case "Register":
        return (<Register {...globalNavigatorProps} />)
      case "NewAppointment":
        return (<NewAppointment {...globalNavigatorProps} {...route} />)
      default:
        return (<HomeIndex {...globalNavigatorProps} />)
    }
  }


  render() {
    return (
      <Navigator
        initialRoute={this.props.initialRoute}
        ref="appNavigator"
        style={styles.navigatorStyles}
        renderScene={this._renderScene}
        configureScene={(route) => ({
          ...route.sceneConfig || Navigator.SceneConfigs.HorizontalSwipeJump
        })}
        handleTokenState={(prop) => {
          this.setState({
            isTokenValid: prop
          })
        }}
        mainState={this.state}
      />
    )
  }

}



module.exports = AppNavigator
