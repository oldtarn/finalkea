var crypto = require('crypto');

//PBKDF2 configuration
const config_hash = {
    saltLength: 32,
    hashIterations: 5000,
    keyLength: 64,
    digest: 'sha512'
};

exports.generatePasswordHash = (password_clear, salt) => {
    const s = (salt === undefined) ? crypto.randomBytes(config_hash.saltLength) : Buffer.from(salt, 'base64');
    const hash = crypto.pbkdf2Sync(
        password_clear,
        s,
        config_hash.hashIterations,
        config_hash.keyLength,
        config_hash.digest
    );
    return hash.toString('base64') + "$" + s.toString('base64')
};

exports.checkPasswordHash = (password_clear, password_hash) => {
    const [hash, salt] = password_hash.split('$');
    const computed_hash = this.generatePasswordHash(password_clear, salt);
    return computed_hash === password_hash;
};

exports.checkPasswordStrength = (passwordString) => {
    var strength = 0;
    strength += /[A-Z]+/.test(passwordString) ? 1 : 0;
    strength += /[a-z]+/.test(passwordString) ? 1 : 0;
    strength += /[0-9]+/.test(passwordString) ? 1 : 0;
    strength += /[\W]+/.test(passwordString) ? 1 : 0;
    if (strength > 2){
        return false
    }
    else{
        return true
    }
}