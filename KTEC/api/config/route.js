var db = require('./db')
var queries = require('../app/queries')
var pwhashutil = require('./pwhashutil')
var jwt = require('jsonwebtoken')

//JWT secret key signature
process.env.SKEY = 'cbIFheq9wYY7ms3acvzlAF8+hz2ozuiT4a3uscjZ6o8='

var newUser = {
    id: undefined,
    username: undefined,
    password: undefined
};
var tokenUser = undefined;
var nospace = (string) => {
    return string.replace(/\s+/g, "")

}

//TODO: handle all kinds of responses
var errorRes = {
    400: {
        0: { succes: false, message: 'Data not provided' },
        1: { succes: false, message: 'Wrong data provided' }
    },
    500: {
        succes: false, message: 'Server error'
    }
}



exports.noRoute = (req, res) => {
    res.json({ succes: true, message: 'HTTPS API made with ❤ for KEA' })
}

exports.login = (req, res) => {
    if (!req.body.user || !req.body.pass) {
        res.status(400)
        res.json(errorRes[400][0])
        return
    }

    var postUser = {
        "name": nospace(req.body.user)
    }
    var postPass = nospace(req.body.pass)
    db.query(queries.login, postUser.name, function (err, result) {
        if (err) {
            console.log(err)
            res.status(500)
            res.json(errorRes[500])
        }
        else if (!!result[0] && !!postPass && result.affectedRows != 0) {
            if (pwhashutil.checkPasswordHash(postPass, result[0].password)) {
                //Create JWT token
                var token = jwt.sign(postUser, process.env.SKEY, { expiresIn: 5000 })
                res.json({ succes: true, message: 'Succesful login', "token": token })
            }
            else {
                res.status(400)
                res.json(errorRes[400][1])
            }
        }
        else {
            res.status(400)
            res.json(errorRes[400][1])
        }
    })
}

exports.register = (req, res) => {
    if (!req.body.user || !req.body.pass) {
        res.status(400)
        res.json(errorRes[400][0])
        return
    }
    var inputUsername = nospace(req.body.user)
    var inputPassword = nospace(req.body.pass)
    //Check password strength
    if (inputUsername.length < 5 || inputPassword.length < 8 || pwhashutil.checkPasswordStrength(inputPassword)) {
        res.status(400)
        res.json({ succes: false, message: 'Username and/or password too short/weak' })
        return
    }

    //Hash and insert into DB
    newUser.username = inputUsername
    newUser.password = pwhashutil.generatePasswordHash(inputPassword)
    db.query(queries.register, newUser, function (err, result) {
        if (err) {
            console.log(err)
            res.status(500)
            res.json(errorRes[500])
        }
        else if (result.affectedRows != 0) {
            res.json({ succes: true, message: 'User created' })
        }
        else {
            res.status(400)
            res.json(errorRes[400][1])
        }
    })
}

exports.getTreatments = (req, res) => {
    db.query(queries.treatments, function (err, result) {
        if (err) {
            console.log(err)
            res.status(500)
            res.json(errorRes[500])
        }
        else {
            //treatments name
            // result.forEach(function(element) {
            //    console.log(element.name)
            // }, this);
            res.json(result)
        }
    })
}

//protected route handler - check JWT
exports.userRoute = (req, res, next) => {
    var token = req.body.token || req.headers['token'];
    if (token) {
        jwt.verify(token, process.env.SKEY, (err, decode) => {
            if (err) {
                res.status(401)
                res.send({ succes: false, 'message': 'Invalid token' })
            }
            else {
                tokenUser = decode.name
                next()
            }
        })
    }
    else (
        res.status(400),
        res.send(errorRes[400][1])
    )
}

//DEV: return token for testing purposes
exports.checkToken = (req, res) => {
    var token = req.body.token || req.headers['token']
    res.send({ succes: true, token: token })
}

exports.newProfile = (req, res) => {
    if ((req.body.firstname || req.headers['firstname']) && (req.body.lastname || req.headers['lastname']) && (req.body.address || req.headers['address']) && (req.body.phoneno || req.headers['phoneno'])) {
        var firstname = (req.body.firstname || req.headers['firstname'])
        var lastname = (req.body.lastname || req.headers['lastname'])
        var address = (req.body.address || req.headers['address'])
        var phone = (req.body.phoneno || req.headers['phoneno'])
        var picture = (req.body.picture || req.headers['picture'])
        db.query(queries.newProfile, [firstname, lastname, address, phone, picture, tokenUser], function (err, result) {
            if (err) {
                console.log(err)
                res.status(500)
                res.json(errorRes[500])
            }
            else if (result.affectedRows != 0) {
                res.json({ succes: true, message: "Profile created" })
            }
            else {
                res.status(400)
                res.json(errorRes[400][1])
            }
        })
    }
    else {
        res.status(400)
        res.json(errorRes[400][1])
    }
}

exports.newAppointment = (req, res) => {
    if ((req.body.datetime || req.headers['datetime']) && (req.body.type || req.headers['type'])) {
        var datetime = (req.body.datetime || req.headers['datetime'])
        var type = (req.body.type || req.headers['type'])
        db.query(queries.newAppointment, [datetime, type, tokenUser], function (err, result) {
            if (err) {
                console.log(err)
                res.status(500)
                res.json(errorRes[500])
            }
            else if (result.affectedRows != 0) {
                res.json({ succes: true, message: "Appointment created" })
            }
            else {
                res.status(400)
                res.json(errorRes[400][1])
            }
        })
    }
    else {
        res.status(400)
        res.json(errorRes[400][1])
    }
}


exports.getProfile = (req, res) => {
    db.query(queries.userProfile, tokenUser, function (err, result) {
        if (err) {
            console.log(err)
            res.status(500)
            res.json(errorRes[500])
        }
        else {
            res.json(result)
        }
    })
}

exports.getAppointments = (req, res) => {
    db.query(queries.userAppointments, tokenUser, function (err, result) {
        if (err) {
            console.log(err)
            res.status(500)
            res.json(errorRes[500])
        }
        else {
            res.json(result)
        }
    })
}

exports.editProfile = (req, res) => {
    if ((req.body.oldpass || req.headers['oldpass']) && (req.body.newpass || req.headers['newpass'])) {
        var oldPass = (req.body.oldpass || req.headers['oldpass']).replace(/\s/g, "")
        var newPass = (req.body.newpass || req.headers['newpass']).replace(/\s/g, "")
    }

    if (oldPass && !pwhashutil.checkPasswordStrength(newPass)) {
        db.query(queries.userPassword, tokenUser, function (err, result) {
            if (err) {
                console.log(err)
                res.status(500)
                res.json(errorRes[500])
            }
            else if (!!result[0] && !!oldPass) {
                if (pwhashutil.checkPasswordHash(oldPass, result[0].password)) {
                    db.query(queries.editPassword, [pwhashutil.generatePasswordHash(newPass), tokenUser], function (err) {
                        if (err) {
                            console.log(err)
                            res.status(500)
                            res.json(errorRes[500])
                        }
                        else if (result.affectedRows != 0) {
                            res.json({ succes: true, message: "Password updated" })
                        }
                        else {
                            res.status(400)
                            res.json(errorRes[400][1])
                        }
                    })
                }
                else {
                    res.status(400)
                    res.json(errorRes[400][1])
                }
            }
            else {
                res.status(400)
                res.json(errorRes[400][1])
            }
        })
    }
    else if ((req.body.firstname || req.headers['firstname']) && (req.body.lastname || req.headers['lastname']) && (req.body.address || req.headers['address']) && (req.body.phoneno || req.headers['phoneno'])) {
        var newFirstname = (req.body.firstname || req.headers['firstname'])
        var newLastname = (req.body.lastname || req.headers['lastname'])
        var newAddress = (req.body.address || req.headers['address'])
        var newPhone = (req.body.phoneno || req.headers['phoneno'])
        var newPicture = (req.body.picture || req.headers['picture'])
        db.query(queries.editProfile, [newFirstname, newLastname, newAddress, newPhone, newPicture, tokenUser], function (err, result) {
            if (err) {
                console.log(err)
                res.status(500)
                res.json(errorRes[500])
            }
            else if (result.affectedRows != 0) {
                res.json({ succes: true, message: "Profile updated" })
            }
            else {
                res.status(400)
                res.json(errorRes[400][1])
            }
        })
    }
    else {
        res.status(400)
        res.json(errorRes[400][1])
    }
}

exports.editAppointments = (req, res) => {
    if ((req.body.olddate || req.headers['olddate']) && (req.body.newdate || req.headers['newdate']) && (req.body.newtype || req.headers['newtype'])) {
        var oldDate = (req.body.olddate || req.headers['olddate']).replace(/\s/g, "")
        var newDate = (req.body.newdate || req.headers['newdate']).replace(/\s/g, "")
        var newType = (req.body.newtype || req.headers['newtype']).replace(/\s/g, "")
    }

    if (oldDate && newDate && newType) {
        db.query(queries.editAppointments, [newDate, newType, tokenUser, oldDate], function (err, result) {
            if (err) {
                console.log(err)
                res.status(500)
                res.json(errorRes[500])
            }
            else if (result.affectedRows != 0) {
                res.json({ succes: true, message: "Appointment succesfully edited" })
            }
            else {
                res.status(400)
                res.json(errorRes[400][1])
            }
        })
    }
    else {
        res.status(400)
        res.json(errorRes[400][1])
    }
}

exports.deleteAppointment = (req, res) => {
    if (req.body.datetime || req.headers['datetime']) {
        db.query(queries.deleteAppointment, [tokenUser, req.body.datetime], function (err, result) {
            if (err) {
                console.log(err)
                res.status(500)
                res.json(errorRes[500])
            }
            else if (result.affectedRows != 0) {
                res.json({ succes: true, message: "Appointment deleted" })
            }
            else {
                res.status(400)
                res.json(errorRes[400][1])
            }
        })
    }
    else {
        res.status(400)
        res.json(errorRes[400][1])
    }
}


exports.handle404 = (req, res) => {
    res.status(404).send({
        'succes': 'false',
        'message': 'Page not Found'
    })
}

exports.handle500 = (error, req, res, next) => {
    console.log(error)
    res.status(500).send({
        'succes': 'false',
        'message': 'Server Error'
    })
}