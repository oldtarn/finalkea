var config = require('../config/route.js')
var express = require('express')
var protectedRoutes = express.Router()


module.exports = (app) => {
    app.get('/', config.noRoute)
    app.post('/login', config.login)
    app.post('/register', config.register)
    app.get('/pricelist', config.getTreatments)

    app.use('/user', protectedRoutes)
    protectedRoutes.use(config.userRoute)
    protectedRoutes.get('', config.checkToken)
    protectedRoutes.get('/profile', config.getProfile)
    protectedRoutes.get('/appointment', config.getAppointments)
    protectedRoutes.get('/appointment/:id', config.getAppointments)
    protectedRoutes.post('/profile', config.newProfile)
    protectedRoutes.post('/appointment', config.newAppointment)
    protectedRoutes.put('/profile', config.editProfile)
    protectedRoutes.put('/appointment', config.editAppointments)
    protectedRoutes.delete('/appointment', config.deleteAppointment) 


    //Handle 404 and 500
    app.use(config.handle404)
    app.use(config.handle500)
}