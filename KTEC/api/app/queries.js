exports.register = 'INSERT INTO credentials SET ?'
exports.newProfile = 'INSERT INTO details SELECT null, credentials.id, ?, ?, ?, ?, ? FROM credentials WHERE credentials.username = ?'
exports.newAppointment = 'INSERT INTO appointments SELECT null, credentials.id, ?, ? FROM credentials WHERE credentials.username = ?'


exports.login = 'SELECT credentials.username, credentials.password FROM credentials WHERE credentials.username = ? '
exports.treatments = 'SELECT treatments.name, treatments.price FROM treatments'
exports.treatmentsId = 'SELECT treatments.id FROM treatments'
exports.userProfile = 'SELECT details.firstname, details.lastname, details.address, details.phonenumber, details.profilepicture from details JOIN credentials WHERE details.usr_id = credentials.id AND credentials.username = ?'
exports.userAppointments = 'SELECT treatments.name, appointments.date_time FROM appointments JOIN credentials JOIN treatments WHERE appointments.usr_id= credentials.id AND appointments.type = treatments.id AND credentials.username = ? ORDER BY appointments.date_time'
exports.userPassword = 'SELECT credentials.password FROM credentials WHERE credentials.username = ?'

exports.editPassword = 'UPDATE credentials SET credentials.password = ? WHERE credentials.username = ?'
exports.editProfile = 'UPDATE details INNER JOIN credentials SET details.firstname = ?, details.lastname = ?, details.address = ?, details.phonenumber = ?, details.profilepicture = ?  WHERE details.usr_id = credentials.id AND credentials.username = ?'
exports.editAppointments = 'UPDATE appointments INNER JOIN credentials SET appointments.date_time = ?, appointments.type= ? WHERE appointments.usr_id = credentials.id AND credentials.username = ? AND appointments.date_time = ?'

exports.deleteAppointment = 'DELETE appointments FROM appointments INNER JOIN credentials ON credentials.id=appointments.usr_id WHERE credentials.username = ? AND appointments.date_time = ?' 