var express = require('express')
var app = express()
var port = process.env.PORT || 3033

//access request data
var bodyParser = require('body-parser')
//console requests info
var morgan = require('morgan')
//security headers
var helmet = require('helmet')
const https = require('https')
//access filesystems
const fs = require('fs');
const httpsConfig = {
    key: fs.readFileSync('./config/cert/key.pem'),
    cert: fs.readFileSync('./config/cert/cert.pem')
}

//disable powered by express - security reasons
//app.disable('x-powered-by')
app.use(helmet())
app.use(morgan('dev'))
app.use(bodyParser.json({
    type: function () {
        return true;
    }
}));

require('./app/routes.js')(app);

const server = https.createServer(httpsConfig, app).listen(port, () => {
    console.log('Initializing server...\nLocalizing resources...\nReady to use @ https://nodeapi.ch:' + port);
})